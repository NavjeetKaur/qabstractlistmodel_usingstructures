﻿#ifndef MODEL_H
#define MODEL_H
#include "listmodel.h"

struct Ingredient
{
    int id = 0;
    QString name;
    int count;

    QVariant data(int role) const;
    bool setData(const QVariant &v, int role);
};

class IngredientModel : public EditableListModel<Ingredient>
{
    Q_OBJECT

public:
    enum Roles {
        ID = Qt::UserRole + 1,
        Name,
        Count
    };
    Q_ENUM(Roles)

    explicit IngredientModel(QObject *parent = nullptr);
};

struct Recipe
{
    int id = 0;
    QString description;
    int count;
    IngredientModel *ingredient = nullptr;

    QVariant data(int role) const;
    bool setData(const QVariant &v, int role);
};

class RecipeModel : public EditableListModel<Recipe>
{
    Q_OBJECT

public:
    enum Roles {
        ID = Qt::UserRole + 1,
        Description,
        Count,
        Ingredient
    };
    Q_ENUM(Roles)

    explicit RecipeModel(QObject *parent = nullptr);
};


struct Category
{
    QString name;
    RecipeModel *recipes = nullptr;

    QVariant data(int role) const;
    bool setData(const QVariant &v, int role);
};

class CategoryModel : public EditableListModel<Category>
{
    Q_OBJECT

public:
    enum Roles {
        Name = Qt::UserRole + 1,
        Recipe
    };
    Q_ENUM(Roles)

    explicit CategoryModel(QObject *parent = nullptr);
};

#endif // MODEL_H
