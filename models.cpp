#include "models.h"

CategoryModel::CategoryModel(QObject *parent)
    : EditableListModel<Category>(parent)
{
}

//This function takes the role and returns the QVariant
QVariant Category::data(int role) const
{
    switch (role) {
    case CategoryModel::Name:
        return name;
    case CategoryModel::Recipe:
        return QVariant::fromValue(static_cast<QObject *>(recipes));
    default:
        return QVariant();
    }
}

//This Function updates the role with the variant recieved
bool Category::setData(const QVariant &v, int role)
{
    switch (role) {
    case CategoryModel::Name:
        name = v.toString();
        return true;
    default:
        return false;
    }
}


RecipeModel::RecipeModel(QObject *parent)
    : EditableListModel<Recipe>(parent)
{
}

//This function takes the role and returns the QVariant
QVariant Recipe::data(int role) const
{
    switch (role) {
    case RecipeModel::ID:
        return id;
    case RecipeModel::Description:
        return description;
    case RecipeModel::Count:
        return count;
    case RecipeModel::Ingredient:
        return QVariant::fromValue(static_cast<QObject *>(ingredient));
    default:
        return QVariant();
    }
}

//This Function updates the role with the variant recieved
bool Recipe::setData(const QVariant &v, int role)
{
    switch (role) {
    case RecipeModel::ID:
        id = v.toInt();
        return true;
    case RecipeModel::Description:
        description = v.toString();
        return true;
    case RecipeModel::Count:
        count = v.toInt();
        return true;
    default:
        return false;
    }
}

IngredientModel::IngredientModel(QObject *parent)
    : EditableListModel<Ingredient>(parent)
{
    qDebug() << Q_FUNC_INFO;
}

//This Function updates the role with the variant recieved
QVariant Ingredient::data(int role) const
{
    qDebug() << Q_FUNC_INFO << role << "*****" << name << id << count;
    switch (role) {
    case IngredientModel::ID:
        return id;
    case IngredientModel::Name:
        qDebug() << Q_FUNC_INFO << name;
        return name;
    case IngredientModel::Count:
        return count;
    default:
        return QVariant();
    }
}

//This Function updates the role with the variant recieved
bool Ingredient::setData(const QVariant &v, int role)
{
    switch (role) {
    case IngredientModel::ID:
        id = v.toInt();
        return true;
    case IngredientModel::Name:
        name = v.toString();
        return true;
    case IngredientModel::Count:
        count = v.toInt();
        return true;
    default:
        return false;
    }
}
