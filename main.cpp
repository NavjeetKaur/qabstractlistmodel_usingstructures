#include <QQuickView>
#include <QString>
#include "models.h"
#include <QGuiApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    // Fill out data model here
    CategoryModel myModel;

    Category p1;
    p1.name = QStringLiteral("Appetizer");
    p1.recipes = new RecipeModel(&myModel);

    Recipe t11;
    t11.id = 1;
    t11.description = QStringLiteral("Cheese Cake");
    t11.count = 20;
    t11.ingredient = new IngredientModel(&myModel);

    Ingredient i11;
    i11.name = "Cheese";
    i11.id = 1;
    i11.count = 5;
    //t11.ingredient->data(QModelIndex(),i11.count);

    Ingredient i12;
    i12.name = "Sugar";
    i12.id = 2;
    i12.count = 10;

    Ingredient i13;
    i13.name = "Flour";
    i13.id = 3;
    i13.count = 15;

    t11.ingredient->insertRow(i11);
    t11.ingredient->insertRow(i12);
    t11.ingredient->insertRow(i13);

    p1.recipes->insertRow(t11);

    Recipe t12;
    t12.id = 2;
    t12.description = QStringLiteral("Almond Cake");
    t12.count = 10;
    t12.ingredient = new IngredientModel(&myModel);

    Ingredient i21;
    i21.id = 1;
    i21.name = "Almond";
    i21.count = 5;

    Ingredient i22;
    i22.id = 2;
    i22.name = "Milk";
    i22.count = 1;

    Ingredient i23;
    i23.id = 3;
    i23.name = "Cream";
    i23.count = 15;

    t12.ingredient->insertRow(i21);
    t12.ingredient->insertRow(i22);
    t12.ingredient->insertRow(i23);
    p1.recipes->insertRow(t12);

    Recipe t13;
    t13.id = 3;
    t13.description = QStringLiteral("Fruit Cake");
    t13.count = 13;
    p1.recipes->insertRow(t13);

    Recipe t14;
    t14.id = 4;
    t14.description = QStringLiteral("Chocolate Cake");
    t14.count = 14;
    p1.recipes->insertRow(t14);
    myModel.insertRow(p1);

    Category p2;
    p2.name = QStringLiteral("Dessert");
    // Giving a parent ensures proper ownership to model
    p2.recipes = new RecipeModel(&myModel);

    Recipe t21;
    t21.id = 1;
    t21.description = QStringLiteral("Cheese Roll");
    t21.count = 11;
    p2.recipes->insertRow(t21);

    Recipe t22;
    t22.id = 2;
    t22.description = QStringLiteral("Finger Chips");
    t22.count = 50;
    p2.recipes->insertRow(t22);

    myModel.insertRow(p2);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("myModel", &myModel);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
